import { useState, useEffect } from 'react';



function isValidPassword(password) {
    let regex = /^(?=.*[A-Z])(?=.*[!@#\$%\^&\*])(?=.{8,})/;
    return regex.test(password);
}

export default function usePasswordValidation() {
    const [password, setPassword] = useState('');
    const [password1, setPassword1] = useState('');
    const [isValid, setIsValid] = useState(false);
    const [passwordIsValid, setPasswordIsValid] = useState(false);


    useEffect(() => {
     
    const pattern = /^(?=.*[A-Z])(?=.*[!@#$%^&*])(?=.*[0-9])(?=.*[a-z]).{8,}$/;
    setIsValid(pattern.test(password));
  }, [password]);

  return isValid;

 // useEffect(() => {
 //        setPasswordIsValid(UsePasswordValidation(password1));
 //      }, [password1]);

 //      // Check if password is valid and display alert if not
 //      if (!passwordIsValid) {
 //        alert('Password does not meet requirements');
 //        return;
 //      }


    // function handlePasswordChange(e) {
    //     setPassword(e.target.value);
    //     //setPassword1(e.target.value);
    //     //alert("Password must have at least 1 capital letter, 1 special character, and a minimum of 8 characters");
    //     setIsValid(isValidPassword(e.target.value));
    // }

    return { password1, password, isValid};
}
