
import { useState, useEffect,useContext } from 'react';

import {Navigate, useNavigate, Link, NavLink} from 'react-router-dom';

import UserContext from '../UserContext';

import { Form, Button, Table } from 'react-bootstrap';

import Swal from 'sweetalert2';




import CreateOrderView from '../components/CreateOrderView';




	export default function AdminRole() {
		

	// Get user context to check if user is admin
	const { user } = useContext(UserContext);
	// Use navigate hook to redirect user
	const navigate = useNavigate();
	// State to store all products
	const [products, setProducts] = useState([]);
	// State for creating new product
	const [title, setTitle] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState("");
	const [isActive, setIsActive] = useState(true);
	const [productId, setProductId] = useState("");



	// Function to create new product
	const createProduct = (e) => {
    	e.preventDefault();
    	// Check if user is admin
    	if (user.isAdmin) {
        	fetch(`${process.env.REACT_APP_API_URL}/products/createProduct`, {
           	 method: "POST",
            	headers: {
            		'Content-Type': 'application/json',
            		Authorization: `Bearer ${localStorage.getItem('token')}`
            	},
           		body: JSON.stringify({
            		title: title,
           	 		description: description,
            		price: price,
            		isActive: isActive,
           			productId: productId
           		 })
            })
            .then(res => res.json())
            .then(data => {
            	console.log(data);
             if(data !== null){
                    // Clear input fields
                    setTitle("");
                    setDescription("");
                    setPrice("");

                    Swal.fire({
                      title: "Product created successfully",
                      icon: "success",
                      text: "Your product has been created and is now available for purchase."
                    })

                    navigate("/products")

                  } else {
                    Swal.fire({
                      title: "Error creating product",
                      icon: "error",
                      text: "There was an error creating your product. Please try again."
                    })
                  }
                })
            }
        }    

        const activeProduct = (e) => {
            e.preventDefault();
            // Check if user is admin
            if (user.isAdmin) {
            	fetch(`${process.env.REACT_APP_API_URL}/products/activeProduct`, {
            	headers: {
            	Authorization: `Bearer ${localStorage.getItem('token')}`
            		}
            	})
            	.then(res => res.json())
            	.then(data => {
           			 console.log(data);
            		setProducts(data);
           		 })
     
            } 
          };

			
	

	return (

	<div>
	<h1>Admin Dashboard</h1>
		<Button variant="primary" onClick={createProduct}>Create Product</Button>
		<Button variant="info" onClick={activeProduct}>Retrieve All active Products</Button>
	<Table>
		<thead>
			<tr>
				<th>#</th>
				<th>Title</th>
				<th>Description</th>
				<th>Price</th>
				<th>Active</th>
			</tr>
		</thead>
		<tbody>
			{
	    products.map((product, index) => (
	        <tr key={product._id}>
	            <td>{index + 1}</td>
	            <td>{product.title}</td>
	            <td>{product.description}</td>
	            <td>{product.price}</td>
	            <td>{product.isActive ? "Yes" : "No"}</td>
	        </tr>
	    ))
			}
		</tbody>
	</Table>
</div>
	)
}
